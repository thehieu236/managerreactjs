import React, { Component } from 'react';

class Search extends Component {

	constructor(props){
		super(props);
		{
			this.state = {
				tempValue: '',
			}
		}
	}
	
	isChange = (e) => {
		this.setState({
			tempValue: e.target.value
		})

		this.props.checkConnectProps(this.state.tempValue)
	};

	render() {
		return (
			<div className="col-12">
				<div className="form-group">
					<div className="btn-group">
						<input
							type="text"
							className="form-control"
							placeholder="Nhập tên sản phẩm,.."
							style={{ width: 531 }}
							onChange={(e) => {
								this.isChange(e);
							}}
						/>
						<button
							className="btn btn-info"
							onClick={(dl) =>this.props.checkConnectProps(this.state.tempValue)}>
							Tìm
						</button>
					</div>
				</div>
				<hr />
			</div>
			
		);
	}
}
export default Search;
