import React, { Component } from "react";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      tel: "",
      permission: "",
      trangThaiChinhSua: true,
    };
  }

  isChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      [name]: value,
    });

    var item = {};
    item.id = this.state.id;
    item.name = this.state.name;
    item.tel = this.state.tel;
    item.permission = this.state.permission;
  };

  thayDoiTrangThai = () => {
    this.setState({
      trangThaiChinhSua: !this.state.trangThaiChinhSua,
    });
  };

  hienThiAddUser = () => {
    if (this.state.trangThaiChinhSua === true) {
      return (
        <div
          className="btn btn-block btn-outline-info"
          onClick={this.thayDoiTrangThai}
        >
          Thêm mới
        </div>
      );
    } else {
      return (
        <form className="card">
          <div className="card-boder-primary mb-3">
            <div className="card-header">Thêm mới vào hệ thồng</div>
            <div className="card-body text-primary">
              <div className="form-group">
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  placeholder="Tên User"
                  onChange={(e) => this.isChange(e)}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="tel"
                  className="form-control"
                  placeholder="Điện thoại"
                  onChange={(e) => this.isChange(e)}
                />
              </div>

              <div
                className="form-group"
                name="permission"
                onChange={(e) => this.isChange(e)}
              >
                <select className="custom-select">
                  <option value>Chọn quyền mặc định</option>
                  <option value={1}>Admin</option>
                  <option value={2}>Modrator</option>
                  <option value={3}>Normal</option>
                </select>
              </div>
              <div className="form-group">
                <input
                  type="reset"
                  className="btn btn-block btn-primary"
                  onClick={(name, tel, permission) =>
                    this.props.add(
                      this.state.name,
                      this.state.tel,
                      this.state.permission
                    )
                  }
                  value="Thêm mới"
                />
              </div>
              <div
                className="btn btn-block btn-outline-secondary"
                onClick={this.thayDoiTrangThai}
              >
                Đóng lại
              </div>
            </div>
          </div>
        </form>
      );
    }
  };

  render() {
    return <div className="col-3">{this.hienThiAddUser()}</div>;
  }
}

export default AddUser;
