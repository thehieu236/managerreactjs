import React, { Component } from "react";
import AddUser from "./AddUser";
import DataUser from "./Data.json";
import Header from "./Header";
import Search from "./Search";
import TableData from "./TableData";
import { v4 as uuidv4 } from "uuid";

class Manager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: DataUser,
      searchText: "",
    };
  }

  editUser = () => {
    console.log("first");
  };

  getNewUserData = (name, tel, permission) => {
    var item = {};
    item.id = uuidv4();
    item.name = name;
    item.tel = tel;
    item.permission = permission;

    var items = this.state.data;
    items.push(item);

    this.setState({
      data: items,
    });

    console.log(this.state.data);
    console.log(item.permission);
  };

  getTextSearch = (dl) => {
    this.setState({ searchText: dl });
  };

  render() {
    var ketqua = [];
    this.state.data.forEach((item) => {
      if (item.name.indexOf(this.state.searchText) !== -1) {
        ketqua.push(item);
      }
    });
    return (
      <div>
        <Header />
        <div className="searchForm">
          <div className="container">
            <div className="row">
              <Search checkConnectProps={(dl) => this.getTextSearch(dl)} />
            </div>
            <div className="row">
              <TableData
                editFun={() => this.editUser()}
                dataUserProps={ketqua}
              />
              <AddUser
                add={(name, tel, permission) =>
                  this.getNewUserData(name, tel, permission)
                }
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Manager;
